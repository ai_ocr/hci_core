from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
import simplejson
import base64
import re

from ia_ocr.ia.findNumber import FindNumber

def ocr(request):
	template = loader.get_template('index.html')
	context = {}
	return HttpResponse(template.render(context, request))

@csrf_exempt
def ajax_search(request):
	img64 = request.POST.get("imageBase64")
	imgstr = re.search(r'base64,(.*)', img64).group(1)
	imgdata = base64.b64decode(imgstr)
	filename = 'ia_ocr/images/search.png'

	with open(filename, 'wb') as f:
		f.write(imgdata)
		f.close()
	
	data = FindNumber().getNumber( filename )

	return HttpResponse(data, content_type='application/json')