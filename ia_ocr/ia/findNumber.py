import tensorflow as tf
import numpy as np
import scipy.ndimage
import PIL
from PIL import Image
import json
import os

class FindNumber : 
  # Function
  def conv2d(self, x, W):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

  def max_pool_2x2(self, x):
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
  
  def getNumber(self, imagePath) :
    # --------------------------------- Training --------------------------------- #

    # Session
    sess = tf.InteractiveSession()

    # Get the training saved
    absolutePath = os.path.dirname(os.path.abspath(__file__)) + "/"
    pathSave = absolutePath + 'training/model'

    saver = tf.train.import_meta_graph(pathSave+'.meta')
    saver.restore(sess,tf.train.latest_checkpoint(absolutePath + 'training/'))
    sess.run(tf.global_variables_initializer())
    saver.restore(sess, pathSave)

    graph = tf.get_default_graph()

    # Variable
    x = tf.placeholder(tf.float32, shape=[None, 784])

    W_conv1 = graph.get_tensor_by_name("W_conv1:0")
    b_conv1 = graph.get_tensor_by_name("b_conv1:0")

    x_image = tf.reshape(x, [-1,28,28,1])

    h_conv1 = tf.nn.relu(self.conv2d(x_image, W_conv1) + b_conv1)
    h_pool1 = self.max_pool_2x2(h_conv1)

    W_conv2 = graph.get_tensor_by_name("W_conv2:0")
    b_conv2 = graph.get_tensor_by_name("b_conv2:0")

    h_conv2 = tf.nn.relu(self.conv2d(h_pool1, W_conv2) + b_conv2)
    h_pool2 = self.max_pool_2x2(h_conv2)

    W_fc1 = graph.get_tensor_by_name("W_fc1:0")
    b_fc1 = graph.get_tensor_by_name("b_fc1:0")

    h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
    h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

    keep_prob = tf.placeholder(tf.float32)
    h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

    W_fc2 = graph.get_tensor_by_name("W_fc2:0")
    b_fc2 = graph.get_tensor_by_name("b_fc2:0")

    y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2

    # --------------------------------- IMAGE --------------------------------- # 
    # Image Path
    imageTemp = absolutePath + 'temp/resized_image.png'

    # Rezied image to 28x28
    img = Image.open( imagePath )

    basewidth = 28
    wpercent = (basewidth / float(img.size[0]))
    hsize = int((float(img.size[1]) * float(wpercent)))
    img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
    img.save( imageTemp )

    # convert imgage to np.array
    data = np.vectorize(lambda x: 255 - x)(np.ndarray.flatten(scipy.ndimage.imread( imageTemp , flatten=True)))

    # number find
    result = sess.run(tf.argmax(y_conv,1), feed_dict={x: [data], keep_prob:1.0})

    # delete image
    os.remove( imageTemp )

    return json.dumps( { "result" : ' '.join(map(str, result)) } )